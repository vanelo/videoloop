#!/bin/bash
# description: movideoloop system
# processname: movideoloop

do_start () {
    omxplayer --loop --vol 0 --no-osd /home/pi/Videos/video.mp4
}
do_stop () {
    killall omxplayer.bin
}
 
case "$1" in
 
    start|stop)
        do_${1}
        echo " service is started|stopped"
        ;;
 
    restart|reload|force-reload)
        do_stop
        do_start
        echo " service is restart|reload|force-reload"
        ;;
    *)
        echo "Usage: service movideoloop {start|stop|restart}"
        exit 1
        ;;
 
esac
exit 0
